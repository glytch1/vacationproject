﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    public Material[] MeshMaterials;
    public Material DamageFlashColor;
    public GameObject Mesh;
    public Renderer Renderer;
    private Material[] MaterialsToApply;
    private bool isFlashing;
    private void Start()
    {
        Renderer = Mesh.GetComponent<Renderer>();
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
if(!isFlashing){
StartCoroutine(DamageFlash());}
        }
    }

    private IEnumerator DamageFlash()

    {       isFlashing=true;
         Debug.Log("HIT!");
MaterialsToApply= Renderer.materials;
        MaterialsToApply[0] = DamageFlashColor;
        MaterialsToApply[1] = DamageFlashColor;
        Renderer.materials=MaterialsToApply;
        yield return new WaitForSeconds(0.075f);
        MaterialsToApply[0] = MeshMaterials[0];
        MaterialsToApply[1] = MeshMaterials[1];
        Renderer.materials=MaterialsToApply;
        yield return new WaitForSeconds(0.075f);
        MaterialsToApply[0] = DamageFlashColor;
        MaterialsToApply[1] = DamageFlashColor;
        Renderer.materials=MaterialsToApply;
        yield return new WaitForSeconds(0.075f);
        MaterialsToApply[0] = MeshMaterials[0];
        MaterialsToApply[1] = MeshMaterials[1];
        Renderer.materials=MaterialsToApply;
        yield return new WaitForSeconds(0.075f);
        MaterialsToApply[0] = DamageFlashColor;
        MaterialsToApply[1] = DamageFlashColor;
        Renderer.materials=MaterialsToApply;
        yield return new WaitForSeconds(0.075f);
        MaterialsToApply[0] = MeshMaterials[0];
        MaterialsToApply[1] = MeshMaterials[1];
        yield return new WaitForSeconds(0.075f);
        Renderer.materials=MaterialsToApply;
        Debug.Log("HIT END!");
        isFlashing=false;
        yield break;
    }
}
