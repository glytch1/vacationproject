﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

        public bool canShoot;
        public GameObject[] Turrets;
        private GameObject CurrentFiringTurret;
        public bool isBallActive;

    
private void Start() {
    Turrets= GameObject.FindGameObjectsWithTag("Enemy");
}
    void Update()
    {
        if(!isBallActive){
            CurrentFiringTurret=Turrets[Random.Range(0,Turrets.Length-1)];
            if(CurrentFiringTurret!=null){
            CurrentFiringTurret.GetComponent<EnemyController>().FireNormal();
            isBallActive=true;}
        }

    }
}
