﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform Target;
    public Vector3 offset;
    public Vector3 Debug;
    private float xrot;
    private float yrot;
    public float Hsens;
    public float Vsens;
    public float Vlimit;
void LateUpdate()
{
    Debug= transform.rotation.eulerAngles;
    xrot+=Input.GetAxis("Mouse X");
    yrot-=Input.GetAxis("Mouse Y");
    yrot= Mathf.Clamp(yrot,-90+Vlimit,90-Vlimit);
}
     void FixedUpdate()
    {
        Vector3 dir = offset;
        Quaternion rotation = Quaternion.Euler(yrot,xrot,0);
        transform.position= Target.position+rotation*dir;
        transform.LookAt(Target);
    }
}
