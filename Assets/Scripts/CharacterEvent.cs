﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterEvent : MonoBehaviour
{
    public Animator AnimatorController;
    public GameObject Player;
    public void ResetSwingLayerWeight()
    {
        AnimatorController.SetLayerWeight(2, 0);
    }
    public void ResetDodgeLayerWeight()
    {
        AnimatorController.SetLayerWeight(1, 0);
    }
    public void DisableHitbox()
    {
        //Player.GetComponent<CapsuleCollider>().enabled=false;
    }
    public void EnableHitbox()
    {
        //Player.GetComponent<CapsuleCollider>().enabled=true;
    }
}
