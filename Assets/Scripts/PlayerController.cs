﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum Playerstate
    {
        IDLE,
        RUNNING,
        JUMPING,
        SWINGING,
        DODGING,
    }

    public float GravityScale;
    public Rigidbody Rb;
    public Animator AnimatorController;
    public Vector3 MovementDirection;
    public float Accel;
    public float MaxSpeed;
    public float Dodgespeed;
    private Vector3 CurrentSpeed;
    public float JumpForce;
    public float SwingStrenght;
    public bool isGrounded = true;
    public float SwingControlSpeed;
    private float CharacterRotation;
    public GameObject Ball;
    private bool canJump = true;
    private bool canDodge = true;
    public float RaycastDistance;
    // Start is called before the first frame update
    void start()
    {
        Rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        CheckGround();
        SwingControlSpeed = transform.InverseTransformDirection(Rb.velocity).x;
        Swing();
        UpdateAnim();
        Movement();
        Jump();
        Gravity();
        CharacterRotation += Input.GetAxis("Mouse X");
        //if (Input.GetButtonDown("Fire1"))
        //{
        //    Instantiate(Ball, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Quaternion.identity);
        //}
    }
    private void LateUpdate()
    {
        Quaternion Rotation = Quaternion.Euler(0, CharacterRotation, 0);
        transform.rotation = Rotation;
    }
    public void Movement()
    {

        if (Input.GetAxisRaw("Horizontal") != 0 | Input.GetAxisRaw("Vertical") != 0)
        {

            MovementDirection = (transform.forward * Input.GetAxis("Vertical") * Accel) + (transform.right * Input.GetAxis("Horizontal") * Accel);
            if (Rb.velocity.x > MaxSpeed)
            {
                Rb.AddForce(Rb.velocity.x, Rb.velocity.y, MovementDirection.z);
            }

            else if (Rb.velocity.z > MaxSpeed)
            {
                Rb.AddForce(MovementDirection.x, Rb.velocity.y, Rb.velocity.z);
            }
            else
            {
                Rb.AddForce(MovementDirection.x, Rb.velocity.y, MovementDirection.z);
            }

            //Rb.velocity = new Vector3(Input.GetAxis("Horizontal")*Movespeed*transform.forward.x,Rb.velocity.y, Input.GetAxis("Vertical")*Movespeed*transform.forward.z);
            CurrentSpeed = Rb.velocity;
        }

    }
    public void UpdateAnim()
    {
        AnimatorController.SetFloat("PosX", Input.GetAxis("Horizontal"));
        AnimatorController.SetFloat("PosY", Input.GetAxis("Vertical"));
    }
    public void Swing()
    {
        if (Input.GetButton("Fire1"))
        {

            AnimatorController.SetLayerWeight(2, 1);
            SwingControlSpeed = transform.InverseTransformDirection(Rb.velocity).x;
            if (SwingControlSpeed < -0.1f)
            {
                AnimatorController.SetTrigger("SwingLeft");
            }
            else
            {

                AnimatorController.SetTrigger("Swing");
            }
        }

    }
    public void Jump()
    {
        if (Input.GetButton("Jump"))
        {
            if (Input.GetAxisRaw("Horizontal") != 0 && canDodge)
            {
                Vector3 dir = transform.forward * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");
                Rb.AddForce(dir.x * Dodgespeed, 0, dir.z * Dodgespeed, ForceMode.Impulse);
                if (Input.GetAxisRaw("Horizontal") > 0)
                {

                    AnimatorController.SetLayerWeight(1, 1);
                    AnimatorController.SetTrigger("DodgeRight");
                }
                else if (Input.GetAxisRaw("Horizontal") < 0)
                {

                    AnimatorController.SetLayerWeight(1, 1);
                    AnimatorController.SetTrigger("DodgeLeft");
                }
                StartCoroutine(DodgeReset());
            }
            else if (Input.GetAxisRaw("Horizontal") == 0 && isGrounded && canJump)
            {
                Rb.AddForce(transform.up * JumpForce, ForceMode.Impulse);
                AnimatorController.SetTrigger("Jump");
                StartCoroutine(JumpReset());
            }
        }
    }
    public void Gravity()
    {
        if (!isGrounded)
        {
            Rb.AddForce(Physics.gravity * GravityScale);
        }
    }
    private void OnCollisionEnter(Collision other)
    {
        isGrounded = true;
        AnimatorController.SetBool("Grounded", true);
        Rb.velocity = new Vector3(Rb.velocity.x, 0, Rb.velocity.z);
    }
    private void OnCollisionExit(Collision other)
    {
        isGrounded = false;
        AnimatorController.SetBool("Grounded", false);
    }
    private IEnumerator JumpReset()
    {
        canJump = false;
        yield return new WaitForSeconds(0.1f);
        canJump = true;
    }
    private IEnumerator DodgeReset()
    {
        canDodge = false;
        yield return new WaitForSeconds(1f);
        canDodge = true;
    }
    public void ToggleSlowmo(){

    }
    public void CheckGround()
    {
        Debug.DrawRay(transform.position, Vector3.down*RaycastDistance,Color.green);
        bool hit = Physics.Raycast(transform.position, Vector3.down, RaycastDistance, LayerMask.NameToLayer("Ground"), QueryTriggerInteraction.Ignore);
        if (hit)
        {
            Debug.Log("Ground HIT");
            isGrounded = true;
        }
        else
        {
            Debug.Log("Ground MISS");
            isGrounded = false;
        }

    }
}

