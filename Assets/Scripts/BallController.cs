﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public enum BallMode
    {
        NORMAL,
        HEAVY,
        GUIDED
    }
    public BallMode BallType;
    private GameManager GameManager;
    public GameObject CameraHandle;
    public GameObject Player;
    private bool hit = false;
    public float StartSpeed;
    public float MaxSpeed;
    public float TurnSpeed;

    public GameObject[] Targets;
    private Rigidbody rb;
    private float DistanceFromClosestTarget = 0;
    public float DistanceOffset;
    public GameObject CurrentTarget;

    public Vector3 LocalTargetSpeed;
    // Start is called before the first frame update
    void Start()
    {
        GameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        Player = GameObject.FindGameObjectWithTag("Player");
        DistanceFromClosestTarget = Mathf.Infinity;
        rb = GetComponent<Rigidbody>();
        CameraHandle = GameObject.Find("Main Camera");
        if (BallType == BallMode.NORMAL)
        {
            CurrentTarget = GameObject.FindGameObjectWithTag("DefendableObject");
            Vector3 dir= CurrentTarget.transform.position-transform.position;
            dir=CurrentTarget.transform.InverseTransformDirection(dir);
            float Angle= Mathf.Atan2(dir.y,dir.x)*Mathf.Rad2Deg;
            Debug.Log(Angle);
            Debug.Log(dir);
            float distance = Vector3.Distance(this.gameObject.transform.position, CurrentTarget.gameObject.transform.position);
            Debug.Log(distance);
            transform.LookAt(CurrentTarget.gameObject.transform);
            float FireAngle = 0.5f * Mathf.Asin(((Physics.gravity.y* -1) * (distance-DistanceOffset)) / Mathf.Pow(StartSpeed, 2));
            Debug.Log(0.5f * ((Physics.gravity.y * -1) * distance) / Mathf.Pow(StartSpeed, 2));
            Debug.Log((FireAngle * 180) / Mathf.PI);
            transform.Rotate(new Vector3(-FireAngle*Mathf.Rad2Deg, 0, 0));
            Debug.Log("Expected distance: "+ (Mathf.Pow(StartSpeed,2)*Mathf.Sin(FireAngle*2))/Physics.gravity.y);
            //transform.rotation = Quaternion.Euler(transform.rotation.x+(FireAngle*180)/Mathf.PI, transform.rotation.y, transform.rotation.z);
            Vector3 PushForce = StartSpeed * transform.forward;
            rb.AddForce(PushForce, ForceMode.Impulse);
            Debug.Log(rb.velocity);
        }
        else if (BallType == BallMode.GUIDED)
        {



            rb.AddForce(1, 15, -13, ForceMode.Impulse);

            CurrentTarget = Player;
            LocalTargetSpeed = CurrentTarget.transform.position;
        }

    }
    void FixedUpdate()
    {
        if (BallType == BallMode.GUIDED)
        {
            LocalTargetSpeed = (CurrentTarget.GetComponent<Rigidbody>().velocity * (Vector3.Distance(CurrentTarget.transform.position, this.transform.position) / rb.velocity.magnitude));
            //Vector3 LocalTargetSpeed = new Vector3(CurrentTarget.GetComponent<Rigidbody>().velocity.x * (CurrentTarget.transform.position.x - this.transform.position.x) / rb.velocity.x, CurrentTarget.GetComponent<Rigidbody>().velocity.y * (CurrentTarget.transform.position.y - this.transform.position.y) / rb.velocity.y, CurrentTarget.GetComponent<Rigidbody>().velocity.z * (CurrentTarget.transform.position.z - this.transform.position.z) / rb.velocity.z);

            rb.velocity = Vector3.ClampMagnitude(rb.velocity, MaxSpeed);
            //+new Vector3(CurrentTarget.GetComponent<Rigidbody>().velocity.x*((CurrentTarget.transform.position.x-this.transform.position.x)/rb.velocity.x),CurrentTarget.GetComponent<Rigidbody>().velocity.y*((this.transform.position.y-CurrentTarget.transform.position.y)/rb.velocity.y),CurrentTarget.GetComponent<Rigidbody>().velocity.z*((this.transform.position.z-CurrentTarget.transform.position.z)/rb.velocity.z))
            if (LocalTargetSpeed.x != float.NaN || LocalTargetSpeed.y != float.NaN || LocalTargetSpeed.z != float.NaN)
            {
                transform.LookAt((CurrentTarget.transform.position + LocalTargetSpeed));
            }
            rb.AddForce(new Vector3(transform.forward.x, transform.forward.y + (transform.up.y * 0.3f), transform.forward.z) * TurnSpeed, ForceMode.Acceleration);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "BatSwingHitbox")
        {
            Debug.Log(CameraHandle.transform.forward);
            StartCoroutine(HitFreeze());
            hit = true;
            rb.useGravity = true;
            rb.velocity = Vector3.zero;
            rb.AddForce(new Vector3(CameraHandle.transform.forward.x, CameraHandle.transform.forward.y + 0.45f - CameraHandle.transform.forward.y, CameraHandle.transform.forward.z) * other.transform.parent.parent.parent.GetComponent<PlayerController>().SwingStrenght, ForceMode.Impulse);
            Targets = GameObject.FindGameObjectsWithTag("Enemy");
            for (int i = 0; i < Targets.Length; i++)
            {
                Debug.Log(Vector3.Distance(this.transform.position, Targets[i].transform.position));
                Debug.Log(DistanceFromClosestTarget);
                if (Vector3.Distance(this.transform.position, Targets[i].transform.position) < DistanceFromClosestTarget)
                {
                    Debug.Log("Reach");
                    DistanceFromClosestTarget = Vector3.Distance(this.transform.position, Targets[i].transform.position);
                    CurrentTarget = Targets[i];

                }
            }

        }
    }
    private void OnCollisionEnter(Collision other)
    {
        //if (other.gameObject.tag == "Enemy")
        //{
        //   Destroy(other.gameObject);
        //   Destroy(gameObject);


        //  GameManager.isBallActive = false;
        //}
    }
    public IEnumerator HitFreeze(float seconds = 0.66f)
    {
        Time.timeScale = 0.1f;
        yield return new WaitForSecondsRealtime(seconds);
        Time.timeScale = 1f;
        yield break;
    }
}

